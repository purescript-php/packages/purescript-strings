<?php

$exports['_localeCompare'] = function ($lt) {
  return function ($eq) use (&$lt) {
    return function ($gt) use (&$qt, &$lt) {
      return function ($s1) use (&$gt, &$qt, &$lt) {
        return function ($s2) use (&$s1, &$gt, &$qt, &$lt) {
          $result = strcmp($s1, $s2);
          return $result < 0 ? $lt : $result > 0 ? $gt : $eq;
        };
      };
    };
  };
};

$exports['replace'] = function ($search) {
  return function ($replace) use (&$search) {
    return function ($subject) use (&$search, &$replace) {
      $pos = strpos($subject, $search);
      if ($pos !== false) {
          return substr_replace($subject, $replace, $pos, strlen($search));
      }
      return $subject;
    };
  };
};

$exports['replaceAll'] = function ($search) {
  return function ($replace) use (&$search) {
    return function ($subject) use (&$replace, &$search) {
      return str_replace($search, $replace, $subject);
    };
  };
};

$exports['split'] = function ($sep) {
  return function ($s) use (&$sep) {
    return explode($sep, $s);
  };
};

$exports['toLower'] = function ($s) {
  return strtolower($s);
};

$exports['toUpper'] = function ($s) {
  return strtoupper($s);
};

$exports['trim'] = function ($s) {
  return trim($s);
};

$exports['joinWith'] = function ($s) {
  return function ($xs) use (&$s) {
    return implode($s, $xs);
  };
};
