<?php

$exports["showRegex'"] = function ($r) {
  return "" + r;
};

$exports["regex'"] = function ($left) {
  return function ($right) use (&$left) {
    return function ($s1) use (&$left, &$right) {
      return function ($s2) use (&$left, &$right, &$s1) {
        try {
          //TODO test if exp is valid!
          return $right(["exp" => $s1, "flags" => $s2]);
        } catch (Exception $e) {
          return $left($e->getMessage());
        }
      };
    };
  };
};

$exports['source'] = function ($r) {
  return $r['exp'];
};

$exports["flags'"] = function ($r) {
  $flags = $r['flags'];
  return [
    'multiline' => $flags['multiline'],
    'ignoreCase' => $flags['ignoreCase'],
    'global' => $flags['global'],
    'sticky' => $flags['sticky'],
    'unicode' => $flags['unicode']
  ];
};

$exports['test'] = function ($r) {
  return function ($s) {
    if ($r['flags']['global']) {
      
    }
    var lastIndex = r.lastIndex;
    var result = r.test(s);
    r.lastIndex = lastIndex;
    return result;
  };
};

$exports['_match'] = function ($just) {
  return function (nothing) {
    return function (r) {
      return function (s) {
        var m = s.match(r);
        if (m == null || m.length === 0) {
          return nothing;
        } else {
          for (var i = 0; i < m.length; i++) {
            m[i] = m[i] == null ? nothing : just(m[i]);
          }
          return just(m);
        }
      };
    };
  };
};

$exports['replace'] = function ($r) {
  return function (s1) {
    return function (s2) {
      return s2.replace(r, s1);
    };
  };
};

$exports["replace'"] = function ($r) {
  return function (f) {
    return function (s2) {
      return s2.replace(r, function (match) {
        return f(match)(Array.prototype.splice.call(arguments, 1, arguments.length - 3));
      });
    };
  };
};

$exports['_search'] = function ($just) {
  return function (nothing) {
    return function (r) {
      return function (s) {
        var result = s.search(r);
        return result === -1 ? nothing : just(result);
      };
    };
  };
};

$exports['split'] = function ($r) {
  return function (s) {
    return s.split(r);
  };
};
