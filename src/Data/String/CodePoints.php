<?php

$hasArrayFrom = typeof Array.from === "function";
$hasStringIterator =
  typeof Symbol !== "undefined" &&
  Symbol != null &&
  typeof Symbol.iterator !== "undefined" &&
  typeof String.prototype[Symbol.iterator] === "function";
$hasFromCodePoint = typeof String.prototype.fromCodePoint === "function";
$hasCodePointAt = typeof String.prototype.codePointAt === "function";

$exports['_unsafeCodePointAt0'] = function ($fallback) {
  return $fallback; //TODO var_dump(IntlChar::getNumericValue("\u{216C}"));
  // return hasCodePointAt
  //   ? function (str) { return str.codePointAt(0); }
  //   : fallback;
};

$exports['_codePointAt'] = function ($fallback) {
  return function ($Just) use (&$fallback) {
    return function ($Nothing) use (&$Just, &$fallback)  {
      return function ($unsafeCodePointAt0) use (&$Nothing, &$Just, &$fallback)  {
        return function ($index) use (&$unsafeCodePointAt0, &$Nothing, &$Just, &$fallback) {
          return function ($str) use (&$index, &$unsafeCodePointAt0, &$Nothing, &$Just, &$fallback) {
            // TODO 
            // $length = str.length;
            // if (index < 0 || index >= length) return Nothing;
            // if (hasStringIterator) {
            //   $iter = str[Symbol.iterator]();
            //   for ($i = index;; --i) {
            //     $o = iter.next();
            //     if (o.done) return Nothing;
            //     if (i === 0) return Just(unsafeCodePointAt0(o.value));
            //   }
            // }
            return $fallback($index)($str);
          };
        };
      };
    };
  };
};

$exports['_countPrefix'] = function ($fallback) {
  return function ($unsafeCodePointAt0) use (&$fallback) {
    // TODO
    // if (hasStringIterator) {
    //   return function (pred) {
    //     return function (str) {
    //       $iter = str[Symbol.iterator]();
    //       for ($cpCount = 0; ; ++cpCount) {
    //         $o = iter.next();
    //         if (o.done) return cpCount;
    //         $cp = unsafeCodePointAt0(o.value);
    //         if (!pred(cp)) return cpCount;
    //       }
    //     };
    //   };
    // }
    return $fallback;
  };
};

$exports['_fromCodePointArray'] = function ($singleton) {
  function utf8($num) {
      if($num<=0x7F)       return chr($num);
      if($num<=0x7FF)      return chr(($num>>6)+192).chr(($num&63)+128);
      if($num<=0xFFFF)     return chr(($num>>12)+224).chr((($num>>6)&63)+128).chr(($num&63)+128);
      if($num<=0x1FFFFF)   return chr(($num>>18)+240).chr((($num>>12)&63)+128).chr((($num>>6)&63)+128).chr(($num&63)+128);
      return '';
  }
  return function($cps) {
    $str = "";
    foreach ($cps as $cp) {
      $str .= utf8($cp);
    }
  }
  // return hasFromCodePoint
  // ? function (cps) {
  //   // Function.prototype.apply will fail for very large second parameters,
  //   // so we don't use it for arrays with 10,000 or more entries.
  //   if (cps.length < 10e3) {
  //     return String.fromCodePoint.apply(String, cps);
  //   }
  //   return cps.map(singleton).join("");
  // }
  // : function (cps) {
  //   return cps.map(singleton).join("");
  // };
};

$exports['_singleton'] = function ($fallback) {
  return function($num) {
    if($num<=0x7F)       return chr($num);
    if($num<=0x7FF)      return chr(($num>>6)+192).chr(($num&63)+128);
    if($num<=0xFFFF)     return chr(($num>>12)+224).chr((($num>>6)&63)+128).chr(($num&63)+128);
    if($num<=0x1FFFFF)   return chr(($num>>18)+240).chr((($num>>12)&63)+128).chr((($num>>6)&63)+128).chr(($num&63)+128);
    return '';
  }
};

$exports['_take'] = function ($fallback) {
  return function ($n) use (&$fallback) {
    if (extension_loaded('mbstring')) {
      return function ($str) use (&$fallback, &$n) {
        return mb_substr($str, $n);
      }
    }
    return $fallback($n);
  };
};

$exports['_toCodePointArray'] = function ($fallback) {
  return function ($unsafeCodePointAt0) {
    // if (hasArrayFrom) {
    //   return function (str) {
    //     return Array.from(str, unsafeCodePointAt0);
    //   };
    // }
    return $fallback();
  };
};
