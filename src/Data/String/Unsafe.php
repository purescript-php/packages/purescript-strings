<?php

$exports['charAt'] = function ($i) {
  return function ($s) use (&$i) {
    if ($i >= 0 && $i < count($s)) return substr($s, $i,  1);
    throw new Exception("Data.String.Unsafe.charAt: Invalid index.");
  };
};

$exports['char'] = function ($s) {
  if (count($s) === 1) return substr($s, 0, 1);
  throw new Exception("Data.String.Unsafe.char: Expected string of length 1.");
};
